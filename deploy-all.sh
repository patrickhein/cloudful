#!/bin/bash
set -euo pipefail

base_directory="$1"

for directory in "$base_directory"/*/; do
  echo ""

  compose=$(basename "$directory")
  echo deploying "$compose"

  set -a
  env_file="environments/${compose}.env"

  if [[ -f "$env_file" ]]; then
    echo "loading environment"
    source "$env_file"
  fi
  set +a

  docker-compose -f "${directory}/docker-compose.yml" pull
  docker-compose -f "${directory}/docker-compose.yml" up --remove-orphans -d
done
