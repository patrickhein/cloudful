```sh
zfs set compression=zstd<-level>
zfs set acltype=posixacl <pool>
zfs set xattr=sa <pool>
zfs set relatime=on <pool>
```
